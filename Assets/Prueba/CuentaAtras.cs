using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CuentaAtras : MonoBehaviour
{
    public GameObject boton;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(Cuenta(10));
    }

    // Update is called once per frame
    void Update()
    {

    }

    public IEnumerator Cuenta(int a)
    {
        for (int i = 0; i < a; i++)
        {
            yield return new WaitForSeconds(1);
            print(i);
        }
        boton.SetActive(true);
    }
}
