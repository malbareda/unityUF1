using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CosaPooleable : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        this.GetComponent<Rigidbody2D>().velocity = this.transform.right*Random.Range(1,5);
    }

    // Update is called once per frame
    void Update()
    {
        if (this.transform.position.x > 10)
        {
            this.gameObject.SetActive(false);
        }
    }
}
