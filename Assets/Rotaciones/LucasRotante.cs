using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LucasRotante : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        this.transform.eulerAngles = new Vector3(0, 0, Random.Range(0,360));
        float tiempo = Random.Range(0, 0.05f);
        float tiempoMax = Random.Range(2, 8f);
        StartCoroutine(SpinMeRound(tiempo, tiempoMax));
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public IEnumerator SpinMeRound(float tiempo, float tiempoMax)
    {
        //en Euler los angulos van
        // 0 a 360
        float acc = 0;
        while (acc<=tiempoMax)
        {
            yield return new WaitForSeconds(tiempo);
            acc += tiempo;
            this.transform.Rotate(new Vector3(0, 0, 1));
        }
        lanzarse();
    }

    public void lanzarse()
    {
        this.GetComponent<Rigidbody2D>().velocity = this.transform.up * 9;

    }

    public void lanzarseDirigido(Vector3 a)
    {
        this.transform.up = a - this.transform.position;
        this.GetComponent<Rigidbody2D>().velocity = this.transform.up * 9;

    }
}
