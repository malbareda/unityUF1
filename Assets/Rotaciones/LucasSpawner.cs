using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LucasSpawner : MonoBehaviour
{
    public GameObject lucas;
    public GameObject marc;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(Spawn());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public IEnumerator Spawn()
    {
        while (true)
        {
            GameObject newLucas = Instantiate(lucas);
            marc.GetComponent<moverBasico>().onLucas += newLucas.GetComponent<LucasRotante>().lanzarseDirigido;
            int a = Random.Range(0, 4);
            if (a == 0)
            {
                newLucas.transform.position = new Vector3(Random.Range(-8f, 8f), 4, 0);
            }
            else if (a == 1)
            {
                newLucas.transform.position = new Vector3(Random.Range(-8f, 8f), -4, 0);
            }
            else if (a == 2)
            {
                newLucas.transform.position = new Vector3(-8, Random.Range(-4f, 4f), 0);
            }
            else
            {
                newLucas.transform.position = new Vector3(8, Random.Range(-4f, 4f), 0);
            }
            yield return new WaitForSeconds(0.3f);
        }
    }
}
