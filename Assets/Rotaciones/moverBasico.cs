using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moverBasico : MonoBehaviour
{
    public int spd;
    public delegate void mandameUnLucas(Vector3 a);
    public event mandameUnLucas onLucas;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(quieroLucas());
    }

    public IEnumerator quieroLucas()
    {
        while (true)
        {
            yield return new WaitForSeconds(10);
            onLucas?.Invoke(this.transform.position);
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.W))
        {
            this.GetComponent<Rigidbody2D>().velocity = Vector3.up*spd;
        }else if (Input.GetKey(KeyCode.S))
        {
            this.GetComponent<Rigidbody2D>().velocity = -Vector3.up * spd;

        }
        else if (Input.GetKey(KeyCode.D))
        {
            this.GetComponent<Rigidbody2D>().velocity = Vector3.right * spd;

        }
        else if (Input.GetKey(KeyCode.A)) {
            this.GetComponent<Rigidbody2D>().velocity = -Vector3.right * spd;
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        this.GetComponent<AudioSource>().Play();
        //Destroy(this.gameObject);
    }
}
