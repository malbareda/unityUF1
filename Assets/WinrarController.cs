using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinrarController : MonoBehaviour
{

    public delegate void Punto(int p);
    public event Punto OnPunto;



    public int spd;
    // Start is called before the first frame update
    void Start()
    {
        this.GetComponent<Rigidbody2D>().velocity = new Vector2(spd, 0);
    }

    // Update is called once per frame
    void Update()
    {
        if (this.transform.position.x >= 12)
        {
            this.gameObject.SetActive(false);

        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "malo")
        {
            
            Destroy(collision.gameObject);

            
            OnPunto?.Invoke(1);
            this.gameObject.SetActive(false);
        }
        if (collision.tag == "marc")
        {
            collision.GetComponent<MarcController>().spd *= 2;
            collision.GetComponent<MarcController>().enfadado();
            collision.GetComponent<SpriteRenderer>().color = Color.red;

            OnPunto?.Invoke(10);
            this.gameObject.SetActive(false);

        }
    }
}
