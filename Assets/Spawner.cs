using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{

    public GameObject[] enemigos;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(crearEnemigo());
    }

    // Update is called once per frame
    void Update()
    {
        if (this.transform.position.x < -10)
        {
            Destroy(this.gameObject);
        }
    }

    //para que sea una corrutina
    IEnumerator crearEnemigo()
    {
        while (true)
        {
            GameObject newEnemigo = Instantiate(enemigos[Random.Range(0,2)]);
            newEnemigo.transform.position = new Vector3(this.transform.position.x, Random.Range(-2f, 4f), 0);
            //esto es lo que esperas es lo mismo que un Thread.Sleep
            int tiempoEspera = Random.Range(2, 5);
            yield return new WaitForSeconds(tiempoEspera);
        }
    }
}
