using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuienEsMiPadre : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag=="Lucas")
        {
            print(this.gameObject.name + " X " + collision.gameObject.name);
            this.transform.parent.GetComponent<YoSoyTuPadre>().cambioColor(this.name);
        }
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag == "Lucas")
            print(this.gameObject.name + " X " + collision.gameObject.name);

    }
}
