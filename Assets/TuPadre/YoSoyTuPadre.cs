using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class YoSoyTuPadre : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.transform.tag == "Lucas")
            print("trigger"+this.gameObject.name + " X " + other.gameObject.name);

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag == "Lucas")
            print("collision"+this.gameObject.name + " X " + collision.gameObject.name);

    }



    public void cambioColor(string s)
    {
        if (s == "Hijo")
            this.GetComponent<SpriteRenderer>().color = Color.blue;
        else if (s == "I'mStuckStepBrother")
        {
            this.GetComponent<SpriteRenderer>().color = Color.red;
        }else if(s == "nieto")
        {
            this.GetComponent<SpriteRenderer>().color = Color.green;
        }
    }
}
