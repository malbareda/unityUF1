using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parallax : MonoBehaviour
{
    public int spd;
    [SerializeField]
    Transform spawn;
    // Start is called before the first frame update
    void Start()
    {
        this.GetComponent<Rigidbody2D>().velocity = Vector3.left* spd;
    }

    // Update is called once per frame
    void Update()
    {
        if (this.transform.position.x < -20)
        {
            this.transform.position = spawn.position;
        }  
    }
}
