using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    int puntos;
    // Start is called before the first frame update
    void Start()
    {
        puntos = 0;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void HazAlgo(int p)
    {
        puntos+=p;
        this.GetComponent<TMPro.TextMeshProUGUI>().text = "Puntos: " + puntos;
    }
}
