using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LucasControllerSinPool : MonoBehaviour
{
    //como es publico...
    public int spd;
    public GameObject winrar;
    private bool cooldown = false;
    public UIManager UIManager;
    public int atontado = 1;

    public Sprite Eric;
    public Sprite Lucas;
    // Start is called before the first frame update
    void Start()
    {
        cooldown = false;
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKey(KeyCode.A))
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(-spd* atontado, this.GetComponent<Rigidbody2D>().velocity.y);
        }
        else if (Input.GetKey(KeyCode.D))
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(spd* atontado, this.GetComponent<Rigidbody2D>().velocity.y);
        }
        else
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, this.GetComponent<Rigidbody2D>().velocity.y);
        }

        if (Input.GetKey(KeyCode.L))
        {
            this.GetComponent<SpriteRenderer>().sprite = Eric;
        }
        else
        {
            this.GetComponent<SpriteRenderer>().sprite = Lucas;
        }


        if (Input.GetKeyDown(KeyCode.Space)){
		    this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0,500));
	    }
        if (Input.GetKeyDown(KeyCode.Z))
        {
            if (!cooldown) { 
                GameObject rar = Instantiate(winrar);
                rar.transform.position = this.transform.position;
                rar.GetComponent<WinrarController>().OnPunto += UIManager.HazAlgo;
                cooldown = true;
                StartCoroutine(Cooldown());
            }
        }
    }

    IEnumerator Cooldown()
    {
        yield return new WaitForSeconds(1);
        cooldown = false;
    }



    private void OnTriggerEnter2D(Collider2D collision)
    {
        print("lucas ha chocado contra " + collision.name);
        if (collision.tag == "malo")
        {
            print("lucas ha roto con Eric");
            //un cambio de escena borra toda la anterior escena
            SceneManager.LoadScene("GameOver");
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag == "marc")
        {
            print("lucas ha suspendido");
            SceneManager.LoadScene("GameOver");
        }
        if (collision.transform.tag == "malo")
        {
            print("muy comoda por cierto");
            //un cambio de escena borra toda la anterior escena
            SceneManager.LoadScene("GameOver");
        }
    }


    public void atontarse()
    {
        atontado = -1;
        StartCoroutine(desatontarse());
    }

    public IEnumerator desatontarse()
    {
        yield return new WaitForSeconds(5);
        atontado = 1;
    }
}
