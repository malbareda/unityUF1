using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossContoller : MonoBehaviour
{
    public int vidaInicial = 100;
    public int vida = 100;
    private float tamaņoBarraVidaInicial;
    public GameObject silla;
    //referencia
    public GameObject barraDeVida;
    public GameObject lucas;
    public GameObject texto;
    public GameObject Eric;

    public delegate void AtraerErics(Transform t);
    public event AtraerErics EricEvent;
    // Start is called before the first frame update
    void Start()
    {
        tamaņoBarraVidaInicial = this.barraDeVida.transform.localScale.x;
        StartCoroutine(atacar());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "disparo")
        {
            print(vida * tamaņoBarraVidaInicial / vidaInicial);
            vida--;
            this.barraDeVida.transform.localScale = new Vector3(vida * tamaņoBarraVidaInicial / vidaInicial, 1,1);
            if (vida == 0)
            {
                Destroy(this.gameObject);
            }
        }
    }

    public IEnumerator atacar()
    {
        while (true)
        {
            yield return new WaitForSeconds(3);
            switch (Random.Range(0, 7))
            {
                case 0:
                    print("WAAAA");
                    lucas.GetComponent<LucasController>().atontarse();
                    texto.SetActive(true);
                    StartCoroutine(desactivarTexto());
                    break;
                case 1:
                    print("tiro una silla");
                    GameObject muyComoda = Instantiate(silla);
                    muyComoda.transform.position = this.transform.position;
                    muyComoda.GetComponent<Rigidbody2D>().velocity = Vector2.left * 2;
                    break;
                case 2:
                case 3:
                case 4:
                case 5:
                    print("creo un eric");
                    GameObject newEric = Instantiate(Eric);
                    newEric.transform.position = new Vector3(Random.Range(-9f, 4f), 4, 0);
                    EricEvent += newEric.GetComponent<EricController>().irALucas;
                    break;
                case 6:
                    print("lcas atrae a eric");
                    EricEvent?.Invoke(lucas.transform);
                    break;

            }
        }
    }

    public IEnumerator desactivarTexto()
    {
       yield return new WaitForSeconds(2);
        texto.SetActive(false);
    }
}
