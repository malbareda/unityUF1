using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EricController : MonoBehaviour
{
    public Transform posLucas;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void irALucas(Transform t)
    {
        Vector2 vecDist = this.transform.position - t.position;
        //NORMALIZAR
        vecDist.Normalize();
        this.GetComponent<Rigidbody2D>().velocity = - vecDist*2;


    }
}
