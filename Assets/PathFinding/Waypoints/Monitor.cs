using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class Monitor : MonoBehaviour
{
    public List<Transform> waypoints = new List<Transform>();
    int actual = 0;
    public int spd;
    // Start is called before the first frame update
    void Start()
    {
        Vector3 nuevoWaypoint = waypoints[actual].position;
        Vector3 vDist = (nuevoWaypoint - this.transform.position).normalized;
        this.GetComponent<Rigidbody2D>().velocity = vDist * spd;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 wpactual = waypoints[actual].position;
        float dist = Vector3.Distance(wpactual,this.transform.position);
        if(dist < 0.5f && actual < waypoints.Count)
        {
            actual++;
            Vector3 nuevoWaypoint = waypoints[actual].position;
            Vector3 vDist = (nuevoWaypoint-this.transform.position).normalized;
            this.GetComponent<Rigidbody2D>().velocity = vDist*spd;

        }
    }
}
