using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JoanEstasParaElArrastre : MonoBehaviour
{
    bool arr = false;
    // Start is called before the first frame update
    private void OnMouseDrag()
    {
        arr = true;
        Vector3 cositas = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        this.transform.position = new Vector3(cositas.x, cositas.y, 0);
    }

    private void OnMouseUp()
    {
        arr = false;
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        print("detecto la colision");
        if (!arr)
        {
            collision.gameObject.SetActive(false);
        }
    }
}
