using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SillaTactica : MonoBehaviour
{
    //encima tactica
    public GameObject proyectil;
    public TacticaSO[] encimaTactica;
    int indice = 0;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(shoot());
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            if (indice < 2) indice++;
        }else if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            if(indice>0) indice--;
        }
    }

    public IEnumerator shoot()
    {
        while (true)
        {
            TacticaSO encimaTactica = this.encimaTactica[indice];
            yield return new WaitForSeconds(encimaTactica.cadencia);
            for (int i = 0;i<encimaTactica.nProy; i++)
            {
                print("a");

                GameObject newProyectil = Instantiate(proyectil);
                newProyectil.transform.position = this.transform.position;
                newProyectil.transform.localScale = newProyectil.transform.localScale * encimaTactica.proysize;
                newProyectil.transform.Rotate(0, 0, Random.Range(-encimaTactica.angDispersion, +encimaTactica.angDispersion));
                newProyectil.GetComponent<Rigidbody2D>().velocity = newProyectil.transform.right * encimaTactica.proySpd;
                newProyectil.GetComponent<SpriteRenderer>().sprite = encimaTactica.sprite;
                newProyectil.GetComponent<SpriteRenderer>().color = encimaTactica.color;

            }

        }
    }
}
