using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class TacticaSO : ScriptableObject
{
    public float proySpd;
    public float cadencia;
    public float proysize;
    public float angDispersion;
    public int nProy;
    public Sprite sprite;
    public Color color;
}
