using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SOGameOver : MonoBehaviour
{

    public VidaLucasSO vida;
    // Start is called before the first frame update
    void Start()
    {
        this.GetComponent<TMPro.TextMeshProUGUI>().text = "Game Over " + vida.hp + " / " + vida.hpMax;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
