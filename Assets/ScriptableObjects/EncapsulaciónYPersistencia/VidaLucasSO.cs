using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu]
public class VidaLucasSO : ScriptableObject
{
    public int hp;
    public int hpMax;
}
