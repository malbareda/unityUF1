using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SOEncCanvas : MonoBehaviour
{
    public VidaLucasSO vida;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        this.GetComponent<TMPro.TextMeshProUGUI>().text = vida.hp + " / " + vida.hpMax;
    }
}
