using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SOEncLucas : MonoBehaviour
{
    public VidaLucasSO vida;
    // Start is called before the first frame update
    void Start()
    {

        vida.hpMax = 10;
        vida.hp = 10;
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            vida.hp++;
            if (vida.hp > vida.hpMax) vida.hp = vida.hpMax;
        }
        else if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            vida.hp--;
            if (vida.hp == 0)
            {
                SceneManager.LoadScene("GameOverSO");
                this.gameObject.SetActive(false);
            }
        }
    }
}
