using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LucasGameEvent : MonoBehaviour
{
    public GameEvent lucasClick;
    bool enfadado;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnMouseDown()
    {
        if (!enfadado)
        {

            enfadado = true;
            this.GetComponent<SpriteRenderer>().color = Color.red;
        }
        else
        {
            this.GetComponent<SpriteRenderer>().color = Color.white;
            enfadado=false; 
        }
            
        lucasClick.Raise();
    }
}
