using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerBase : MonoBehaviour
{
    public GameObject b;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(a());
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public IEnumerator a()
    {
        while (true)
        {
            GameObject n = Instantiate(b);
            yield return new WaitForSeconds(1);
        }
    }
}
