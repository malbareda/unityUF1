using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextEventos3 : MonoBehaviour
{
    private int p = 0;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void actualizarPunt(int p)
    {
        this.p += p;
        this.GetComponent<TMPro.TextMeshProUGUI>().text = "PUNTOS: "+this.p.ToString();
    }
}
