using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemicEventos3 : MonoBehaviour
{
    public GameEventInt gameEventInt;
    private int rand;
    // Start is called before the first frame update
    void Start()
    {
        rand = Random.Range(0, 10);
        this.transform.GetChild(0).GetComponent<TMPro.TextMeshPro>().text = rand.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnMouseDown()
    {
        gameEventInt.Raise(rand);
        this.GetComponent<AudioSource>().Play();
        this.transform.GetChild(1).GetComponent<ParticleSystem>().Play();
        this.GetComponent<Rigidbody2D>().velocity = Vector3.zero;
        this.GetComponent<SpriteRenderer>().enabled = false;
        this.GetComponent<Collider2D>().enabled = false;
        StartCoroutine(kill());
    }

    public IEnumerator kill()
    {
        yield return new WaitForSeconds(2);
        Destroy(gameObject);
    }
}
