using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EricGameEvent : MonoBehaviour
{
    public Transform lucas;
    bool huyendo = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 direccion = lucas.position - transform.position;
        direccion.Normalize();
        if (!huyendo) { 
            this.GetComponent<Rigidbody2D>().velocity = direccion;
        }
        else
        {
            this.GetComponent<Rigidbody2D>().velocity = -direccion;
        }
    }

    public void hazAlgo()
    {
        huyendo = !huyendo;
    }



}
