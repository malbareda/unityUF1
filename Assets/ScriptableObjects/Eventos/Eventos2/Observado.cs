using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Observado : MonoBehaviour
{
    public GameEventTransform alarma;
    public GameEvent llave;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            llave.Raise();
        }
        if (Input.GetKeyDown(KeyCode.B))
        {
            alarma.Raise(this.transform);
        }
    }
}
