using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuardiEvento : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void activaGuardia()
    {
        this.GetComponent<SpriteRenderer>().color = Color.red;
    }

    public void activaGuardiaAlarma(Transform t)
    {
        this.GetComponent<SpriteRenderer>().color = Color.red;
        this.GetComponent<Rigidbody2D>().velocity = (t.position - this.transform.position).normalized;
    }
}
