using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraLucasController : MonoBehaviour
{
    public Transform lucas;
    float minX=0;
    public int camSpd;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        /* camara que no puede ir hacia atras
        if(lucas.transform.position.x>minX)minX = lucas.transform.position.x;
        //v1
        if(lucas.transform.position.x >= minX)
        {
            this.transform.position = new Vector3(lucas.transform.position.x, this.transform.position.y, -10);
        }
        */
        Vector3 lucasPositionRetrasado = new Vector3(lucas.transform.position.x, lucas.transform.position.y, -10);
        this.transform.position = new Vector3(Mathf.Lerp(this.transform.position.x, lucas.transform.position.x, Time.deltaTime * camSpd), this.transform.position.y, -10);

    }
}
